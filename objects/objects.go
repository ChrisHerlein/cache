package objects

import (
	"fmt"
	"sync"
)

type object struct {
	Key   string
	Value interface{}
}

type objKeyRels struct {
	Objs    []object
	Keys    []string
	Created bool
}

//tree contains the object struct. First index represent
//the length of the object key, and second index is the
//ascii representation of the first character of the key.
var tree [51][27]objKeyRels
var creatingLock sync.Mutex

//FindObject will look into the assigned tree and
//will lookup for the given key
func FindObject(key string) (obj interface{}, e error) {
	var keySize = len(key)
	if keySize > 50 || keySize < 1 {
		return nil, fmt.Errorf("Invalid key")
	}
	var using = tree[keySize]
	var first = key[0]
	if first > 90 {
		first = first - 32
	}
	first = first - 65
	if first < 0 || first > 25 {
		return nil, fmt.Errorf("Invalid first character for the key")
	}
	objectList := using[first]

	e = fmt.Errorf("Key not found")
Look:
	for i := 0; i < len(objectList.Objs); i++ {
		if objectList.Objs[i].Key == key {
			obj = objectList.Objs[i].Value
			e = nil
			break Look
		}
	}
	return
}

//AddObject will add a new object into its exact position
//into the assigned tree
func AddObject(key string, value interface{}) error {
	var keySize = len(key)
	if keySize > 50 || keySize < 1 {
		return fmt.Errorf("Invalid key")
	}
	var first = key[0]
	if first > 90 {
		first = first - 32
	}
	first = first - 65
	if first < 0 || first > 25 {
		return fmt.Errorf("Invalid first character for the key")
	}
	if _, e := FindObject(key); e == nil {
		return fmt.Errorf("Object key already exists")
	}
	creatingLock.Lock()
	var using = tree[keySize]
	if !using[first].Created {
		using[first].Objs = make([]object, 0)
		using[first].Keys = make([]string, 0)
		using[first].Created = true
	}
	using[first].Objs = append(using[first].Objs, object{Key: key, Value: value})
	using[first].Keys = append(using[first].Keys, key)
	tree[keySize] = using
	creatingLock.Unlock()
	return nil
}

func DeleteObject(key string) (e error) {
	var keySize = len(key)
	if keySize > 50 || keySize < 1 {
		return fmt.Errorf("Invalid key")
	}
	var using = tree[keySize]
	var first = key[0]
	if first > 90 {
		first = first - 32
	}
	first = first - 65
	if first < 0 || first > 25 {
		return fmt.Errorf("Invalid first character for the key")
	}
	objectList := using[first]

	e = fmt.Errorf("Key not found")
Look:
	for i := 0; i < len(objectList.Objs); i++ {
		if objectList.Objs[i].Key == key {
			creatingLock.Lock()
			tree[keySize][first].Objs = tree[keySize][first].Objs[:i+copy(tree[keySize][first].Objs[i:], tree[keySize][first].Objs[i+1:])]
			tree[keySize][first].Keys = tree[keySize][first].Keys[:i+copy(tree[keySize][first].Keys[i:], tree[keySize][first].Keys[i+1:])]
			creatingLock.Unlock()
			e = nil
			break Look
		}
	}
	return
}
