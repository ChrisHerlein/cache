package context

import (
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
)

var (
	errors     chan error
	bufferSize = 1024
)

//Context is designed to store shared data between handler and controller
type Context struct {
	Auth        string
	IsWebSocket bool
	Body        []byte
	Url         string
	Request     *http.Request
	w           http.ResponseWriter
}

func newContext(w http.ResponseWriter, r *http.Request, needAuth bool) (ctx *Context, e error) {
	var body []byte
	ctx = &Context{}
	ctx.Request = r
	ctx.w = w
	body, _ = ioutil.ReadAll(r.Body)
	ctx.Body = body
	ctx.Url = r.URL.Path
	e = r.Body.Close()
	return ctx, e
}

func (ctx *Context) GetParam(name string) (retval string, ok bool) {
	var value interface{}
	vars := mux.Vars(ctx.Request)
	value, ok = vars[name]

	if ok {
		retval, ok = value.(string)
		return
	}

	vars2 := ctx.Request.URL.Query()
	value, ok = vars2[name]
	if ok {
		retval = string(value.([]string)[0])
	}

	return
}

//SetEChan is the setter to tell context were send errors
func SetEChan(ch chan error) {
	errors = ch
}

//ChangeBufferSize is the setter to change the size
//of the websockets messages buffer.
//Its default is 1024
func ChangeBufferSize(size int) {
	bufferSize = size
}
