package context

import (
	"encoding/json"
	"io"
	"net/http"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  bufferSize,
	WriteBufferSize: bufferSize,
	CheckOrigin:     checkOr,
}

//HandleWs implements http handler interface
//to wrap controllers over a ws connections.
type HandleWs func(ctx *Context) (int, interface{})

//HandleHTTP implements http handler interface
//to wrap controllers over a http connections.
type HandleHTTP func(ctx *Context) (int, interface{})

func (h HandleWs) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	println("in ws handler")
	conn, e := upgrader.Upgrade(w, req, nil)
	if e != nil {
		println("e", e.Error())
		errors <- e
		return
	}
	println("after upgrade")

	var ctx = Context{IsWebSocket: true}
Comunication:
	for {
		println("inside comunication loop")
		_, message, err := conn.ReadMessage()
		if err == nil {
			var body map[string]interface{}
			ctx.Body = message
			e = json.Unmarshal(message, &body)
			url, _ := body["path"].(string)
			if url == "" {
				conn.WriteJSON(map[string]string{"error": "no path set"})
				continue
			}
			ctx.Url = url
			status, response := h(&ctx)
			if response == nil {
				response = map[string]string{"status": "ok"}
			}
			if status >= 300 {
				response = map[string]string{"error": response.(string)}
			}
			err = conn.WriteJSON(response)
		}
		errors <- err
		if err != nil {
			break Comunication
		}
	}
	println("exit handler")
	errors <- conn.Close()
}

func (h HandleHTTP) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	ctx, _ := newContext(w, req, false)
	//writeCrossDomainHeaders(w, req)
	ctx.serveJSON(h(ctx))
}

func (ctx *Context) serveJSON(status int, response interface{}) {
	ctx.w.Header().Set("Content-Type", "application/json")
	if status == 0 {
		status = 200
	}
	ctx.w.WriteHeader(status)
	var jsonResponse []byte
	jsonResponse, _ = json.Marshal(response)
	if response == nil {
		jsonResponse, _ = json.Marshal(map[string]string{"status": "ok"})
	}
	if status > 299 {
		erstr, _ := response.(string)
		jsonResponse, _ = json.Marshal(map[string]string{"error": erstr})
	}
	io.WriteString(ctx.w, string(jsonResponse))
}

func checkOr(r *http.Request) bool {
	return true
}
