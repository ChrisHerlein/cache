package config

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

var cnf customConfig

type customConfig struct {
	Address           string `yaml:"listening.url"`
	Port              int    `yaml:"listening.port"`
	Protocol          string `yaml:"listening.protocol"` //should be "http", "ws" or "unix"
	SocketFile        string `yaml:"listening.socket"`
	EnablePersistance bool   `yaml:"persistance.enable"`
	PersistanceDir    string `yaml:"persistance.directory"`
	ErrorLogFile      string `yaml:"log.file.error"`
	RequestLogFile    string `yaml:"log.file.requests"`
	LogEnabled        bool   `yaml:"log.enabled"`
}

//ReadConfig should be called when server starts.
//It reads a yaml file passed as configFile and parses it in the config struct
func ReadConfig(configFile string) error {
	bf, e := ioutil.ReadFile(configFile)
	if e == nil {
		e = yaml.Unmarshal(bf, &cnf)
	}
	return e
}

//GetAddress is the cnf address getter
func GetAddress() string {
	return cnf.Address
}

//GetAddress is the cnf port getter
func GetPort() int {
	return cnf.Port
}

//GetProtocol is the cnf protocol getter
func GetProtocol() string {
	return cnf.Protocol
}

//GetEnablePersistance is the cnf enable persistance getter
func GetEnablePersistance() bool {
	return cnf.EnablePersistance
}

//GetPersistanceDir is the cnf persistance dir getter
func GetPersistanceDir() string {
	return cnf.PersistanceDir
}

//GetErrorLogFile is the cnf log error file getter
func GetErrorLogFile() string {
	return cnf.ErrorLogFile
}

//GetRequestLogFile is the cnf request log file getter
func GetRequestLogFile() string {
	return cnf.RequestLogFile
}

//GetLogEnabled is the cnf log enabled getter
func GetLogEnabled() bool {
	return cnf.LogEnabled
}

//GetSocket is the socket getter
func GetSocket() string {
	return cnf.SocketFile
}
