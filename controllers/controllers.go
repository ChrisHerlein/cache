package controllers

import (
	"encoding/json"
	"strings"

	"bitbucket.org/chrisherlein/cache/context"
	"bitbucket.org/chrisherlein/cache/objects"
)

type PublicObj struct {
	Key    string      `json:"key"`
	Object interface{} `json:"object"`
}

//SocketDispatcher is the controller used to handle ws requests
//Is the responsable of dispatch http controllers
func SocketDispatcher(ctx *context.Context) (status int, response interface{}) {
	url := strings.Split(ctx.Url, "/")
	if len(url) == 0 {
		status = 400
		response = "No url provided"
		return
	}
	switch url[1] {
	case "create":
		return SetObject(ctx)
	case "read":
		return GetObject(ctx)
	case "remove":
		return RemoveObject(ctx)
	default:
		status = 400
		response = "Invalid URL"
	}
	return
}

//SetObject is the controller that creates a new object and insterts it
//under the objects struct (tbd)
func SetObject(ctx *context.Context) (status int, response interface{}) {
	var newObj PublicObj
	e := json.Unmarshal(ctx.Body, &newObj)
	if e != nil {
		status = 400
		response = e.Error()
		return
	}
	e = objects.AddObject(newObj.Key, newObj.Object)
	if e != nil {
		status = 400
		response = e.Error()
		return
	}
	return
}

//GetObject is the controller that finds an object
//under the objects struct and send it back (tbd)
func GetObject(ctx *context.Context) (status int, response interface{}) {
	var key string
	var ok bool
	var urlParams []string
	urlParams = strings.Split(ctx.Url, "/")
	if len(urlParams) == 3 {
		ok = true
		key = urlParams[2]
	}
	if !ok {
		status = 404
		response = "key not found"
		return
	}
	obj, e := objects.FindObject(key)
	if e != nil {
		status = 400
		response = e.Error()
		return
	}
	response = obj
	return
}

//RemoveObject is the controller that finds an object
//under the objects struct and deletes it from the tree
func RemoveObject(ctx *context.Context) (status int, response interface{}) {
	var key string
	var ok bool
	var urlParams []string
	//if !ctx.IsWebSocket {
	//	key, ok = ctx.GetParam("key")
	//} else {
	urlParams = strings.Split(ctx.Url, "/")
	if len(urlParams) == 3 {
		ok = true
		key = urlParams[2]
	}
	//}
	if !ok {
		status = 404
		response = "key not found"
		return
	}
	var e = objects.DeleteObject(key)
	if e != nil {
		status = 400
		response = map[string]string{"err": e.Error()}
	}
	return
}
