package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"

	"bitbucket.org/chrisherlein/cache/config"
	"bitbucket.org/chrisherlein/cache/context"
	"bitbucket.org/chrisherlein/cache/controllers"
	"bitbucket.org/chrisherlein/cache/errors"

	"github.com/gorilla/mux"
)

var requestLogger *log.Logger
var hand http.Handler

func init() {
	var configFile = flag.String("configFile", "config.yaml", "Where server config file is allocated")
	flag.Parse()
	errors.Pnc <- config.ReadConfig(*configFile)
	var enb = config.GetLogEnabled()
	errors.SetActive(enb)
	if enb {
		initLogs()
	}
	context.SetEChan(errors.Rcv)
}

func initLogs() {
	reqFile, e := os.OpenFile(config.GetRequestLogFile(), os.O_RDWR|os.O_CREATE|os.O_APPEND, 0x644)
	errors.Pnc <- e
	requestLogger = log.New(reqFile, "", 0x644)
	errFile, e := os.OpenFile(config.GetErrorLogFile(), os.O_RDWR|os.O_CREATE|os.O_APPEND, 0x644)
	errors.Pnc <- e
	errors.SetLogger(errFile)
}

func main() {
	var r = mux.NewRouter()
	var protocol = config.GetProtocol()
	switch protocol {
	case "http":
		createHTTP(r)
	case "ws":
		createWs(r)
	case "unix":
		createUnix(r)
	default:
		errors.Pnc <- fmt.Errorf("Unrecognized protocol to launch server: %s", protocol)
	}
}

func createHTTP(r *mux.Router) {
	r.Handle("/create", context.HandleHTTP(controllers.SetObject)).Methods("POST")
	r.Handle("/read/{key}", context.HandleHTTP(controllers.GetObject)).Methods("GET")
	r.Handle("/remove/{key}", context.HandleHTTP(controllers.RemoveObject)).Methods("DELETE")
	http.Handle("/", r)
	listener, err := net.Listen("tcp", fmt.Sprintf("%s:%d", config.GetAddress(), config.GetPort()))
	if err != nil {
		fmt.Printf("Error while starting: %v\n", err.Error())
	}
	fmt.Println("Ready for use")
	http.Serve(listener, nil)
}

func createWs(r *mux.Router) {
	r.Handle("/connect", context.HandleWs(controllers.SocketDispatcher))
	http.Handle("/", r)
	listener, err := net.Listen("tcp", fmt.Sprintf("%s:%d", config.GetAddress(), config.GetPort()))
	if err != nil {
		fmt.Printf("Error while starting: %v\n", err.Error())
	}
	fmt.Println("Ready for use ws")
	println(http.Serve(listener, nil))
}

func createUnix(r *mux.Router) {
	listener, err := net.Listen("unix", config.GetSocket())
	if err != nil {
		fmt.Printf("Error while starting: %v\n", err.Error())
	}
	fmt.Println("Ready for use ws")
	http.Serve(listener, nil)
}
