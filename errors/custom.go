//errors package is designed to handle errors ocurred in the
//server that's being developed. It listens for erros and stop
//the processes if it's necessary.
package errors

import (
	"fmt"
	"io"
	"os"
)

var (
	Pnc    = make(chan error, 1)
	Rcv    = make(chan error, 1)
	End    = make(chan struct{}, 1)
	OK     = make(chan struct{}, 1)
	active = true
	logger io.Writer
)

type defLog struct{}

func (d *defLog) Write(msg []byte) (int, error) {
	return fmt.Println(string(msg))
}

func init() {
	go listenErrors()
	logger = &defLog{}
}

func listenErrors() {
	var e error
Listen:
	for {
		select {
		case e = <-Pnc:
			if e != nil && active {
				fmt.Fprintf(logger, "Fatal error: %s", e.Error())
				os.Exit(2)
			}
		case e = <-Rcv:
			if e != nil && active {
				fmt.Fprintf(logger, "Error: %s", e.Error())
			}
		case <-End:
			break Listen
		}
	}
	OK <- struct{}{}
}

//SetLogger is the setter for change the default logger (screen output)
//for a formal log initialized by the server.
func SetLogger(w io.Writer) {
	logger = w
}

//SetActive is the setter to swap into enable/disable values for the logger.
//If is call with false, then no messages will be sent to logger.
func SetActive(a bool) {
	active = a
}
